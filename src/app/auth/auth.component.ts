import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  isLoginMode = true;

  headers: any;
  constructor(private authService: AuthService) {


  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {

    if (!form.value) {
      //hacked the darn form somehow.
      return;
    }

    console.log(form.value.email)

    const email = form.value.email;
    const password = form.value.password;




    this.authService.login(email, password).subscribe(
      (resp: any) => {

        const keys = resp.headers.keys();
        console.log(keys)

        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);

        console.log("This is the " +
          JSON.stringify(resp));

          console.log(this.headers);

      }
    )
    /* if (this.isLoginMode) {

    } else {
      this.authService.signup(email, password).subscribe(
        (resp: Response) => {
          console.log("This is the " + resp.headers);
        }
      )
    } */


    form.reset();
  }

}
