import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from 'protractor';



@Injectable({ providedIn: 'root' })
export class AuthService {

  private userUrl = 'http://localhost:8080/';

  private header = new Headers();

  constructor(private http: HttpClient) {


  }




  signup(username: string, password: string) {
    return this.http.post(this.userUrl + 'users/signup',
      {
        'username': username,
        'password': password
      }
    );
  }


  login(username: string, password: string): Observable<any> {
    return this.http.post<Config>(this.userUrl + 'login',
      {
        'username': username,
        'password': password
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response'
      }

    );
  }

}
