export interface Userinterface {
    id: number;
    username: string;
    password: string;
    enabled: number;
}
