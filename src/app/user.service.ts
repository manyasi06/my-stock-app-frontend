import { Injectable } from '@angular/core';
import { user } from './user';
import { Userinterface } from './userinterface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {



  private userUrl = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  getUsers(): Observable<any> {
    return this.http.get<any>(this.userUrl);
  }

  createUser(user: user): void {
    this.http.post(this.userUrl, user);
    console.log('Test the user creation:\n' + user + '\n');
  }

  signUp(user: any): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    console.log('This user to be created: ' + user);
    this.http.post(this.userUrl + 'signup/', user, httpOptions).subscribe(
      (resp: any) => {
        console.log(resp);
      }
    ).unsubscribe();
  }

  delete(id: number): void {
    this.http.delete(this.userUrl + id).subscribe(
      (resp: any) => {
        console.log(resp);
      }
    );
  }

}
