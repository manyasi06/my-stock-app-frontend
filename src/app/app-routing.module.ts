import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserlistComponent } from './userlist/userlist.component';
import { AppComponent } from './app.component';
import { CreateuserComponent } from './createuser/createuser.component';
import { AuthComponent } from './auth/auth.component';


const routes: Routes = [
  {path: 'home', component: AppComponent},
  {path: 'users', component: UserlistComponent},
  {path: 'signup', component: CreateuserComponent},
  {path: 'auth', component: AuthComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
