import { Component, OnInit, DoCheck, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { user } from '../user';
import { NgForm } from '@angular/forms';




@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})



export class UserlistComponent implements OnInit {

  public myUsers: user[];
  submitted = false;

  //view the form
  @ViewChild('CreateUser', { static: false }) form: NgForm;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      resp => {
        this.myUsers = resp;
        console.log(this.myUsers)
      }
    )
  }
  onSubmit(f: NgForm){
    let valueofForm = f.value;
    console.log("This is my people: " + JSON.stringify(valueofForm));
    //console.log(this.form);
    this.submitted = true;
  }

  // newPerson(): void {

  //   console.log('New User:' + this.form);
  // }







}
