import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { user } from '../user';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

interface createUser {
  username: string;
  password: string;
};

@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.css']
})
export class CreateuserComponent implements OnInit {

  @ViewChild('signupForm', { static: false }) signupform: NgForm;
  listofUser: Array<user>;


  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      (resp: any) => {
        this.listofUser = resp;
      }
    );
  }

  onSubmit(form: NgForm) {
    const formVal = form.value;

    this.userService.signUp(JSON.stringify(formVal));
    console.log('This is the form: ' + JSON.stringify(formVal));


  }

  deleteAUser(id: number): void {
    this.userService.delete(id);
    location.reload();
  }

}
